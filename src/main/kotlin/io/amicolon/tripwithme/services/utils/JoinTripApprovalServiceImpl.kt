package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.JoinTripApproval
import io.amicolon.tripwithme.repositories.JoinTripApprovalRepository
import org.springframework.stereotype.Service

@Service
class JoinTripApprovalServiceImpl(
        private val joinTripApprovalRepository: JoinTripApprovalRepository
) : JoinTripApprovalService {

    override fun sentRequestToBox(tripId: String, participantId: String, organizerId: String) {
        joinTripApprovalRepository.save(
                JoinTripApproval(
                        organizerId = organizerId,
                        participantId = participantId,
                        tripId = tripId,
                        isPending = true
                )
        )
    }

    override fun getAll(profileId: String): List<JoinTripApproval> {
        return joinTripApprovalRepository.findAllByOrganizerId(profileId.replace("\"", ""))
    }

    override fun deleteMessage(tripId: String, participantId: String) {
        var message = joinTripApprovalRepository.findByParticipantIdAndTripId(participantId, tripId)
        joinTripApprovalRepository.delete(message)
    }
}