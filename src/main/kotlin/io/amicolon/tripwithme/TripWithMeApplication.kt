package io.amicolon.tripwithme

import io.amicolon.tripwithme.domain.*
import io.amicolon.tripwithme.repositories.*
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.crypto.password.PasswordEncoder
import java.time.LocalDate.now
import java.time.LocalDate.of
import java.time.Month
import java.util.Collections.singleton

@SpringBootApplication
class TripWithMeApplication(
        private val tripRepository: TripRepository,
        private val profileRepository: ProfileRepository,
        private val placeRepository: PlaceRepository,
        private val languageRepository: LanguageRepository,
        private val interestRepository: InterestRepository,
        private val genderRepository: GenderRepository,
        private val universityRepository: UniversityRepository,
        private val courseRepository: CourseRepository,
        private val userRepository: UserRepository,
        private val countryRepository: CountryRepository,
        private val roleRepository: RoleRepository,
        private val passwordEncoder: PasswordEncoder
) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {

        if (userRepository.count() < 2) {
            this.insertTravelDetails()
            this.insertDestinations()
            this.insertPersonalDetails()
            roleRepository.insert(Role(role = "ROLE_USER"))

            val overview1 = personalInfo()
            val contactInfo1 = ContactInfo(phoneNumber = "+1378653289", emailAddress = "joe.smith@gmail.com", facebookAccountUrl = "joe.smith")
            val profile1 = profile(overview1, contactInfo1)
            val profile1withId = profileRepository.insert(profile1)
            createUser()

            val overview2 = personalInfo2()
            val contactInfo2 = ContactInfo(phoneNumber = "+44972537218", emailAddress = "daniel.williams@gmail.com", facebookAccountUrl = "daniel.williams")
            val profile2 = profile2(overview2, contactInfo2)
            val profile2withId = profileRepository.insert(profile2)
            createUser2()

            val trips = listOf(mxTrip(profile2withId), ukTrip(profile1withId), usTrip(profile2withId), italyTrip(profile1withId))
            tripRepository.insert(trips)
        }
    }

    private fun personalInfo(): PersonalInfo {
        return PersonalInfo(
                dateOfBirth = of(1987, Month.JANUARY, 14),
                gender = genderRepository.findByName("Male").get(),
                university = universityRepository.findByName("Harvard University").get(),
                course = courseRepository.findByName("Computer Science").get())
    }

    private fun personalInfo2(): PersonalInfo {
        return PersonalInfo(
                dateOfBirth = of(1991, Month.AUGUST, 26),
                gender = genderRepository.findByName("Male").get(),
                university = universityRepository.findByName("Oxford University").get(),
                course = courseRepository.findByName("Chemistry").get())
    }

    private fun profile(overview1: PersonalInfo, contactInfo1: ContactInfo): Profile {
        return Profile(
                id = null,
                name = "Joe",
                surname = "Smith",
                img = "https://i.guim.co.uk/img/media/34bf91085589088e98917c383a559ba14806b928/0_0_3600_4875/master/3600.jpg?width=620&quality=85&auto=format&fit=max&s=4beedb513657f3b26463ff4f5599953d",
                memberSince = of(2019, Month.AUGUST, 24),
                location = placeRepository.findByName("New York City").get(),
                visitedCountries = mutableSetOf(
                        countryRepository.findByName("Mexico").get(),
                        countryRepository.findByName("United States of America").get(),
                        countryRepository.findByName("Spain").get(),
                        countryRepository.findByName("Italy").get(),
                        countryRepository.findByName("United Kingdom").get()
                ),
                knownLanguages = mutableSetOf(
                        languageRepository.findByName("English").get(),
                        languageRepository.findByName("Spanish").get()
                ),
                interests = mutableSetOf(
                        interestRepository.findByName("Hiking").get(),
                        interestRepository.findByName("Sightseeing").get(),
                        interestRepository.findByName("Sea").get()
                ),
                aboutMe = """
                        Curabitur viverra blandit ex. Integer velit risus, ullamcorper vel dictum in, vehicula at neque. Donec finibus blandit tortor at tincidunt. Aliquam venenatis orci sodales vulputate viverra. Integer pretium urna accumsan interdum lacinia. Fusce et malesuada odio. Proin cursus porttitor auctor. Suspendisse tincidunt risus efficitur pharetra facilisis. Suspendisse potenti. Duis maximus risus leo, at hendrerit turpis euismod id. Curabitur ac felis non tellus sodales gravida nec eu nibh. Vestibulum mattis urna sem, non tincidunt neque facilisis eget. Duis elit sem, congue sed ex et, iaculis bibendum justo. Maecenas congue eros erat, a rhoncus ex sollicitudin eu. Aenean a massa dictum, fermentum nunc in, tempus enim.
                    """.trimIndent(),
                personalInfo = overview1,
                contactInfo = contactInfo1
        )
    }

    private fun profile2(overview2: PersonalInfo, contactInfo2: ContactInfo): Profile {
        return Profile(
                id = null,
                name = "Daniel",
                surname = "Williams",
                img = "https://www.biography.com/.image/t_share/MTU1MTM0NzA2NzQ3MTg4NDQw/chris-evans-arrives-at-the-los-angeles-premiere-of-captain-america-the-winter-soldier-held-at-the-el-capitan-theatre-on-march-13-2014-in-hollywood-california-photo-by-michael-tran_filmmagicjpg-square.jpg",
                memberSince = of(2020, Month.APRIL, 13),
                location = placeRepository.findByName("London").get(),
                visitedCountries = mutableSetOf(
                        countryRepository.findByName("Spain").get(),
                        countryRepository.findByName("Italy").get(),
                        countryRepository.findByName("United Kingdom").get(),
                        countryRepository.findByName("France").get(),
                        countryRepository.findByName("Poland").get(),
                        countryRepository.findByName("Germany").get(),
                        countryRepository.findByName("India").get()
                ),
                knownLanguages = mutableSetOf(
                        languageRepository.findByName("English").get(),
                        languageRepository.findByName("French").get(),
                        languageRepository.findByName("German").get()
                ),
                interests = mutableSetOf(
                        interestRepository.findByName("Snorkeling").get(),
                        interestRepository.findByName("Sightseeing").get(),
                        interestRepository.findByName("Sea").get(),
                        interestRepository.findByName("Scuba diving").get()
                ),
                aboutMe = """
                        Curabitur viverra blandit ex. Integer velit risus, ullamcorper vel dictum in, vehicula at neque. Donec finibus blandit tortor at tincidunt. Aliquam venenatis orci sodales vulputate viverra. Integer pretium urna accumsan interdum lacinia. Fusce et malesuada odio. Proin cursus porttitor auctor. Suspendisse tincidunt risus efficitur pharetra facilisis. Suspendisse potenti. Duis maximus risus leo, at hendrerit turpis euismod id. Curabitur ac felis non tellus sodales gravida nec eu nibh. Vestibulum mattis urna sem, non tincidunt neque facilisis eget. Duis elit sem, congue sed ex et, iaculis bibendum justo. Maecenas congue eros erat, a rhoncus ex sollicitudin eu. Aenean a massa dictum, fermentum nunc in, tempus enim.
                    """.trimIndent(),
                personalInfo = overview2,
                contactInfo = contactInfo2
        )
    }

    private fun mxTrip(profile1withId: Profile): Trip {
        return Trip(
                id = null,
                organizer = profile1withId,
                title = "Delicious fajitas and salsa! Trip to mexican land",
                destinations = Destinations(type = "countries", countries = mutableSetOf(countryRepository.findByName("Mexico").get())),
                description = """
                            With 26 UNESCO-declared world heritage sites, charming colonial towns and dozens of thrilling cities, there's plenty to explore in this country of 109 million. Outside the cities, stunning Pacific beaches, stark deserts, mangrove swamps and swimming holes provide all you need for a relaxing, romantic or adventurous vacation. Captivating, cosmopolitan and chaotic Mexico City and the 32 states offer an incredible abundance of experiences, from laid-back and leisurely to upbeat to adrenalin-charging.
                            """.trimIndent(),
                sizeLimit = 4,
                participants = mutableSetOf(),
                budget = 3700,
                startDate = now().plusDays(7),
                finishDate = now().plusDays(24),
                requirements = mutableSetOf(
                        interestRepository.findByName("Don't smoke").get(),
                        interestRepository.findByName("Sea").get(),
                        interestRepository.findByName("Tents").get()
                ),
                img = "https://pix10.agoda.net/geo/city/19741/1_19741_02.jpg?s=1920x822",
                viewCounter = 13
        )
    }

    private fun usTrip(profile1withId: Profile): Trip {
        return Trip(
                id = null,
                organizer = profile1withId,
                title = "Visit Time Square and enjoy american dream!",
                destinations = Destinations(type = "cities", places = mutableSetOf(placeRepository.findByName("New York City").get())),
                description = """
                            Luxe hotels. Gritty dive bars. Broadway magic. Side-street snack carts. Whether you’re a first-time traveler or a long-time resident, NYC is a city that loves to surprise. The unrivaled mix of iconic arts spaces, endless shopping experiences, architectural marvels, and proudly distinct neighborhoods — along with the city’s accessible 24/7 transport — means there’s always more to explore in the five boroughs.
                            """.trimIndent(),
                sizeLimit = 4,
                participants = mutableSetOf(),
                requirements = mutableSetOf(
                        interestRepository.findByName("Sea").get(),
                        interestRepository.findByName("Tents").get()
                ),
                img = "https://s27363.pcdn.co/wp-content/uploads/2018/03/New-York-City-Itinerary-1129x752.jpg.optimal.jpg",
                budget = 1800,
                startDate = now().plusWeeks(16),
                finishDate = now().plusWeeks(18),
                viewCounter = 32
        )
    }

    private fun ukTrip(profile1withId: Profile): Trip {
        return Trip(
                id = null,
                organizer = profile1withId,
                title = "Let's imagine we are kings! Royal journey to United Kingdom's heart",
                destinations = Destinations(type = "cities", places = mutableSetOf(placeRepository.findByName("London").get()), countries = mutableSetOf(countryRepository.findByName("United Kingdom").get())),
                description = """
                                Nothing prepares you for your first taste of London. This great world city is far more than just the capital of the United Kingdom of Great Britain and Northern Ireland. London is bustling, vibrant, multicultural and cosmopolitan. London is both old and new, a place where traditional pubs rub shoulders with the newest cocktail bars. Its most ancient castle is right next door to its newest skyscrapers. London's energy, as 8.6 million people go about their everyday lives, is tangible in every journey on the Underground and every walk along the banks of the River Thames. From the palaces of shopping to the real Buckingham Palace, London really does have it all. London's many attractions have turned it into one of the world's most visited destinations, with some 28 million visitors a year. Whether visitors are seeking culture, shopping, palaces or a great night out, London has it all. London's easy to use public transport systems are another key to its popularity. The London Underground, known universally as "the tube", is simple to understand while the famous red buses are a great way to get to know London at street level. For the indulgent, no visit to London is complete without a ride in a black cab, driven only by those who have completed what's still called "The Knowledge" of all the best ways to avoid London's traffic congestion. London's cab drivers are among this magnificent city's greatest characters.
                            """.trimIndent(),
                sizeLimit = 3,
                participants = mutableSetOf(),
                requirements = mutableSetOf(
                        interestRepository.findByName("Don't smoke").get(),
                        interestRepository.findByName("Sea").get()
                ),
                budget = 3300,
                startDate = now().plusWeeks(4),
                finishDate = now().plusWeeks(5),
                img = "https://cache-graphicslib.viator.com/graphicslib/page-images/742x525/172414_bigbenya.jpg",
                viewCounter = 99
        )
    }

    private fun italyTrip(profile1withId: Profile): Trip {
        return Trip(
                id = null,
                organizer = profile1withId,
                title = "Italian Vacation!",
                destinations = Destinations(type = "cities",
                        places = mutableSetOf(
                                placeRepository.findByName("Milan").get(),
                                placeRepository.findByName("Venice").get(),
                                placeRepository.findByName("Rome").get(),
                                placeRepository.findByName("Florence").get()
                        ),
                        countries = mutableSetOf(countryRepository.findByName("Italy").get())),
                description = """
                            From the silvery crests of the Alps to the sparkling Mediterranean Sea, Italy’s beauty is unparalleled. A visit could mean marveling at the Colosseum after a tender plate of pasta, or drifting lazily down a canal, your belly full of local Sangiovese. Explore Tuscan vineyards and olive groves or drool over boutiques in fashionable Milan. Look for mermaids from the cliffs of Sorrento or nibble on Parma’s famous cheese and prosciutto. Anywhere you choose, the country will captivate you.
                            """.trimIndent(),
                sizeLimit = 3,
                participants = mutableSetOf(),
                requirements = mutableSetOf(
                        interestRepository.findByName("Don't smoke").get(),
                        interestRepository.findByName("Sea").get()
                ),
                budget = 8000,
                startDate = now().plusWeeks(44),
                finishDate = now().plusWeeks(46),
                img = "https://cdn-image.departures.com/sites/default/files/1584458652/header-empty-clean-grand-canal-venice-CLEANVENICE0320.jpg",
                viewCounter = 48
        )
    }

    private fun createUser() {
        val user = User(username = "joesmith", email = "joe.smith@gmail.com", password = "joesmith")
        user.password = passwordEncoder.encode(user.password)
        user.roles = singleton(roleRepository.findByRole("ROLE_USER").get())
        user.profile = profileRepository.findBySurname("Smith").get()
        userRepository.insert(user)
    }

    private fun createUser2() {
        val user2 = User(username = "danielwilliams", email = "daniel.williams@gmail.com", password = "danielwilliams")
        user2.password = passwordEncoder.encode(user2.password)
        user2.roles = singleton(roleRepository.findByRole("ROLE_USER").get())
        user2.profile = profileRepository.findBySurname("Williams").get()
        userRepository.insert(user2)
    }


    private fun insertPersonalDetails() {

        genderRepository.saveAll(mutableListOf(
                Gender(name = "Male"),
                Gender(name = "Female")
        ))

        universityRepository.saveAll(mutableListOf(
                University(name = "Akademia Górniczo-Hutnicza im. Stanisława Staszica"),
                University(name = "Politechnika Krakowska im. Tadeusza Kościuszki"),
                University(name = "Harvard University"),
                University(name = "Oxford University"),
                University(name = "Cambridge University"),
                University(name = "Uniwersytet Jagielloński")
        ))

        courseRepository.saveAll(mutableListOf(
                Course(name = "Computer Science"),
                Course(name = "Civil Engineering"),
                Course(name = "Law"),
                Course(name = "Medicine"),
                Course(name = "Architecture"),
                Course(name = "Chemistry")
        ))

    }

    private fun insertDestinations() {

        countryRepository.saveAll(mutableSetOf(
                Country(name = "Poland"),
                Country(name = "Mexico"),
                Country(name = "Italy"),
                Country(name = "United Kingdom"),
                Country(name = "India"),
                Country(name = "Spain"),
                Country(name = "Germany"),
                Country(name = "China"),
                Country(name = "Japan"),
                Country(name = "Brazil"),
                Country(name = "Canada"),
                Country(name = "Australia"),
                Country(name = "France"),
                Country(name = "Russia"),
                Country(name = "United States of America")

        ))

        placeRepository.saveAll(mutableSetOf(
                Place(name = "Cracow", country = countryRepository.findByName("Poland").get()),
                Place(name = "Warsaw", country = countryRepository.findByName("Poland").get()),
                Place(name = "Berlin", country = countryRepository.findByName("Germany").get()),
                Place(name = "Paris", country = countryRepository.findByName("France").get()),
                Place(name = "Delhi", country = countryRepository.findByName("India").get()),
                Place(name = "Mexico City", country = countryRepository.findByName("Mexico").get()),
                Place(name = "Rio de Janeiro", country = countryRepository.findByName("Brazil").get()),
                Place(name = "Sydney", country = countryRepository.findByName("Australia").get()),
                Place(name = "London", country = countryRepository.findByName("United Kingdom").get()),
                Place(name = "Barcelona", country = countryRepository.findByName("Spain").get()),
                Place(name = "Tokyo", country = countryRepository.findByName("Italy").get()),
                Place(name = "Rome", country = countryRepository.findByName("Italy").get()),
                Place(name = "Venice", country = countryRepository.findByName("Italy").get()),
                Place(name = "Florence", country = countryRepository.findByName("Italy").get()),
                Place(name = "Milan", country = countryRepository.findByName("Italy").get()),
                Place(name = "New York City", country = countryRepository.findByName("United States of America").get()),
                Place(name = "Los Angeles", country = countryRepository.findByName("United States of America").get())
        ))
    }

    private fun insertTravelDetails() {
        languageRepository.saveAll(mutableSetOf(
                Language(name = "Polish"),
                Language(name = "English"),
                Language(name = "Spanish"),
                Language(name = "Chinese"),
                Language(name = "Japanese"),
                Language(name = "Portuguese"),
                Language(name = "French"),
                Language(name = "German"),
                Language(name = "Italian")
        ))

        interestRepository.saveAll(mutableSetOf(
                Interest(name = "Don't drink"),
                Interest(name = "Backpacking"),
                Interest(name = "Hitchhiking"),
                Interest(name = "Road trip"),
                Interest(name = "Climbing"),
                Interest(name = "Mountains"),
                Interest(name = "Sightseeing"),
                Interest(name = "City break"),
                Interest(name = "Scuba diving"),
                Interest(name = "Snorkeling"),
                Interest(name = "Hiking"),
                Interest(name = "Don't smoke"),
                Interest(name = "Sea"),
                Interest(name = "Tents"),
                Interest(name = "Survival")
        ))
    }
}

fun main(args: Array<String>) {
    runApplication<TripWithMeApplication>(*args)
}
