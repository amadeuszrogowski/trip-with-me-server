package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Profile
import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.payloads.responses.UtilsResponse
import io.amicolon.tripwithme.repositories.*
import org.springframework.stereotype.Service

@Service
class UtilsServiceImpl(
        private val genderRepository: GenderRepository,
        private val universityRepository: UniversityRepository,
        private val courseRepository: CourseRepository,
        private val languageRepository: LanguageRepository,
        private val interestRepository: InterestRepository,
        private val placeRepository: PlaceRepository,
        private val countryRepository: CountryRepository,
        private val profileService: ProfileService,
        private val tripService: TripService
) : UtilsService {

    override fun getApprovalOverview(tripId: String, participantId: String): ApprovalOverview {
        return ApprovalOverview(
                trip = tripService.getById(tripId),
                profile = profileService.getById(participantId)
        )
    }

    override fun getAll(): UtilsResponse {
        return UtilsResponse(
                genders = getGenders(),
                courses = getCourses(),
                universities = getUniversities(),
                countries = getCountries(),
                languages = getLanguages(),
                places = getPlaces(),
                activities = getActivities()
        )
    }

    private fun getLanguages(): List<String> {
        return languageRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getCountries(): List<String> {
        return countryRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getUniversities(): List<String> {
        return universityRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getCourses(): List<String> {
        return courseRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getGenders(): List<String> {
        return genderRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getActivities(): List<String> {
        return interestRepository.findAll().asSequence()
                .map { it.name }
                .sorted()
                .toList()
    }

    private fun getPlaces(): List<PlaceWithCountry> {
        return placeRepository.findAll().asSequence()
                .map { PlaceWithCountry(it.name, it.country.name) }
                .sortedBy { it.place }
                .toList()
    }

    class PlaceWithCountry(
            val place: String,
            val country: String
    )

    class ApprovalOverview(
            val trip: Trip,
            val profile: Profile
    )
}