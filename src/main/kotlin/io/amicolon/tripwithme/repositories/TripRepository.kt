package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Trip
import org.springframework.data.mongodb.repository.MongoRepository

interface TripRepository : MongoRepository<Trip, String> {
    fun findAllByOrganizer_Id(id: String) : List<Trip>
}
