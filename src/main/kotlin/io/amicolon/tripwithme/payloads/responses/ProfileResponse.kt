package io.amicolon.tripwithme.payloads.responses

import io.amicolon.tripwithme.domain.Trip
import java.time.LocalDate

data class ProfileResponse(
        val id: String?,
        val name: String?,
        val surname: String?,
        val img: String?,
        val aboutMe: String?,
        val email: String?,
        val facebook: String?,
        val phoneNumber: String?,
        val interests: List<String>,
        val languages: List<String>,
        val countries: List<String>,
        val memberSince: LocalDate,
        val dateOfBirth: LocalDate?,
        val course: String?,
        val gender: String?,
        val location: String?,
        val university: String?,
        val tripsOrganized: List<Trip>,
        val contactVisible: Boolean
)