package io.amicolon.tripwithme.payloads.requests

data class ChangeApprovalStateRequest(
        val profileId: String,
        val tripId: String
)