package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Place (
        @Id val id: String? = null,
        val name: String,
        @DBRef val country: Country
)

@Document
data class Country (
        @Id val id: String? = null,
        val name: String
)