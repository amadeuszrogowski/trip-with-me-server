package io.amicolon.tripwithme.payloads.responses

data class JwtAuthenticationResponse(
        val accessToken: String,
        val userId: String,
        val profileId: String,
        val tokenType: String = "Bearer"
)