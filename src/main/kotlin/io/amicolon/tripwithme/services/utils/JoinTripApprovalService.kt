package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.JoinTripApproval

interface JoinTripApprovalService {
    fun sentRequestToBox(tripId: String, participantId: String, organizerId: String)
    fun getAll(profileId: String) : List<JoinTripApproval>
    fun deleteMessage(tripId: String, participantId: String)
}