package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Interest
import io.amicolon.tripwithme.domain.Language
import io.amicolon.tripwithme.util.FindByName
import org.springframework.data.mongodb.repository.MongoRepository

interface InterestRepository : MongoRepository<Interest, String>, FindByName<Interest, String>

interface LanguageRepository : MongoRepository<Language, String>, FindByName<Language, String>