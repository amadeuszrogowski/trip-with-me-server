package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.payloads.requests.FetchTripsRequest

interface FilterService {
    fun filterBy(trips: List<Trip>, tripRequest: FetchTripsRequest): List<Trip>
}