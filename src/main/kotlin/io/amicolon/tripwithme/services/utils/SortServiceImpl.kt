package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.Trip
import org.springframework.stereotype.Service

@Service
class SortServiceImpl : SortService {

    val sortStrategyMap = mapOf(
            "createdAt::desc" to { trips: List<Trip> -> trips.sortedByDescending { it.createdAt } },
            "budget::asc" to { trips: List<Trip> -> trips.sortedBy { it.budget } },
            "viewCounter::desc" to { trips: List<Trip> -> trips.sortedByDescending { it.viewCounter } },
            "dateFrom::asc" to { trips: List<Trip> -> trips.sortedBy { it.startDate } }
    )

    override fun sortBy(trips: List<Trip>, sortStrategy: String) : List<Trip> {
        return this.sortStrategyMap[sortStrategy]!!.invoke(trips)
    }
}