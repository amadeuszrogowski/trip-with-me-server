package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.services.middlewares.UtilsService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/utils")
class UtilsController(
        private val utilsService: UtilsService
) {
    @GetMapping
    fun getAll() = ResponseEntity.ok().body(utilsService.getAll())

    @GetMapping("/approvaloverview")
    fun getApprovalOverview(@RequestParam tripId: String, @RequestParam participantId: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(utilsService.getApprovalOverview(tripId, participantId))
    }

}