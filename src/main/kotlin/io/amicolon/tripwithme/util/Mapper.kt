package io.amicolon.tripwithme.util

import io.amicolon.tripwithme.domain.Country
import io.amicolon.tripwithme.domain.Interest
import io.amicolon.tripwithme.domain.Language

class Mapper {

    companion object{
        fun <T : Any> mapStringCollectionToSet(collection: String, repository: FindByName<T,String>) : Set<T> {
            return collection.split(",").asSequence()
                    .map { repository.findByName(it) }
                    .map { it.get() }
                    .toSet()
        }

        fun mapCountrySetToStringList(set: Set<Country>) : List<String> {
            return set.asSequence()
                    .map { it.name }
                    .toList()
        }

        fun mapInterestSetToStringList(set: Set<Interest>) : List<String> {
            return set.asSequence()
                    .map { it.name }
                    .toList()
        }

        fun mapLanguageSetToStringList(set: Set<Language>) : List<String> {
            return set.asSequence()
                    .map { it.name }
                    .toList()
        }

    }
}