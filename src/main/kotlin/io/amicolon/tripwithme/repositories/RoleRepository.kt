package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Role
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface RoleRepository : MongoRepository<Role, String> {

    fun findByRole(role: String): Optional<Role>
}