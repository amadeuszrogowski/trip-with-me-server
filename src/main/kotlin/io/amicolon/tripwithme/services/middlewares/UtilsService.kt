package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.payloads.responses.UtilsResponse

interface UtilsService {
    fun getAll() : UtilsResponse
    fun getApprovalOverview(tripId: String, participantId: String): Any?
}