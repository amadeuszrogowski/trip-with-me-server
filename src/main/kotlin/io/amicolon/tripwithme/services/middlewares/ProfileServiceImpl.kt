package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Profile
import io.amicolon.tripwithme.exceptions.ResourceNotFoundException
import io.amicolon.tripwithme.payloads.requests.UpdateProfileRequest
import io.amicolon.tripwithme.payloads.responses.ProfileResponse
import io.amicolon.tripwithme.repositories.*
import io.amicolon.tripwithme.services.utils.JoinTripStateService
import io.amicolon.tripwithme.util.Mapper
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class ProfileServiceImpl(
        private val profileRepository: ProfileRepository,
        private val genderRepository: GenderRepository,
        private val courseRepository: CourseRepository,
        private val universityRepository: UniversityRepository,
        private val languageRepository: LanguageRepository,
        private val countryRepository: CountryRepository,
        private val tripRepository: TripRepository,
        private val placeRepository: PlaceRepository,
        private val joinTripStateService: JoinTripStateService,
        private val interestRepository: InterestRepository
) : ProfileService {
    val resourceName: String = "Profile"

    override fun getAll(): List<Profile> = profileRepository.findAll()

    override fun retrieveById(profileId: String, currentProfileId: String): ProfileResponse {
        val profile = profileRepository.findById(profileId).orElseThrow { ResourceNotFoundException(resourceName, profileId) }

        profile.let {
            return ProfileResponse(
                    id = it.id,
                    name = it.name,
                    surname = it.surname,
                    img = it.img,
                    aboutMe = it.aboutMe,
                    email = it.contactInfo.emailAddress,
                    facebook = it.contactInfo.facebookAccountUrl,
                    phoneNumber = it.contactInfo.phoneNumber,
                    interests = Mapper.mapInterestSetToStringList(it.interests),
                    countries = Mapper.mapCountrySetToStringList(it.visitedCountries),
                    languages = Mapper.mapLanguageSetToStringList(it.knownLanguages),
                    memberSince = it.memberSince,
                    dateOfBirth = it.personalInfo.dateOfBirth,
                    course = it.personalInfo.course?.name,
                    gender = it.personalInfo.gender?.name,
                    university = it.personalInfo.university?.name,
                    location = "${it.location?.name}, ${it.location?.country?.name}",
                    tripsOrganized = tripRepository.findAllByOrganizer_Id(it.id!!),
                    contactVisible = joinTripStateService.isContactDataVisible(profileId, currentProfileId)
            )
        }
    }

    override fun updateById(id: String, updateProfileRequest: UpdateProfileRequest): Profile {

        val profile = profileRepository.findById(id).get()

        profile.contactInfo.apply {
            emailAddress = updateProfileRequest.email
            phoneNumber = updateProfileRequest.phoneNumber
            facebookAccountUrl = updateProfileRequest.facebook
        }

        profile.personalInfo.apply {
            dateOfBirth = LocalDate.parse(updateProfileRequest.dateOfBirth)
            gender = genderRepository.findByName(updateProfileRequest.gender).get()
            course = courseRepository.findByName(updateProfileRequest.course).get()
            university = universityRepository.findByName(updateProfileRequest.university).get()
        }

        profile.apply {
            name = updateProfileRequest.name
            surname = updateProfileRequest.surname
            aboutMe = updateProfileRequest.aboutMe
            img = updateProfileRequest.img
            location = placeRepository.findByName(updateProfileRequest.location.split(",")[0]).get()
            visitedCountries = Mapper.mapStringCollectionToSet(updateProfileRequest.countries, countryRepository)
            knownLanguages = Mapper.mapStringCollectionToSet(updateProfileRequest.languages, languageRepository)
            interests = updateProfileRequest.activities!!.split(",").asSequence()
                    .map { interestRepository.findByName(it).get() }
                    .toSet()
        }

        return profileRepository.save(profile)
    }

    override fun insert(obj: Profile): Profile {
        return TODO("not implemented")
    }

    override fun getById(id: String): Profile {
        return profileRepository.findById(id).get()
    }

    override fun update(obj: Profile): Profile {
        return TODO("not implemented")
    }

    override fun deleteById(id: String): Optional<Profile> {
        return TODO("not implemented")
    }

}