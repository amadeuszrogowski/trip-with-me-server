package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
data class Trip(
        @Id val id: String? = null,
        @DBRef val organizer: Profile,
        var title: String,
        val destinations: Destinations,
        var description: String,
        var budget: Int,
        var startDate: LocalDate,
        var finishDate: LocalDate,
        @DBRef var participants: MutableSet<Profile> = mutableSetOf(),
        var sizeLimit: Int = 0,
        var requirements: Set<Interest> = mutableSetOf(),
        var img: String?,
        val createdAt: LocalDate = LocalDate.now(),
        var viewCounter: Int
)

data class Destinations(
        val type: String,
        @DBRef val countries: Set<Country> = mutableSetOf(),
        @DBRef val places: Set<Place> = mutableSetOf()
)
