package io.amicolon.tripwithme.payloads.requests

data class FetchTripsRequest(
        val sortStrategy: String = "dateFrom::asc",
        val searchType: String?,
        val places: String?,
        val countries: String?,
        val activities: String?,
        val text: String?,
        val startDate: String?,
        val finishDate: String?,
        val minBudget: Int?,
        val maxBudget: Int?,
        val minPeople: Int?,
        val maxPeople: Int?
)