package io.amicolon.tripwithme.payloads.responses

import io.amicolon.tripwithme.services.middlewares.UtilsServiceImpl.PlaceWithCountry

data class UtilsResponse(
        val genders: List<String>,
        val universities: List<String>,
        val courses: List<String>,
        val countries: List<String>,
        val languages: List<String>,
        val activities: List<String>,
        val places: List<PlaceWithCountry>
)