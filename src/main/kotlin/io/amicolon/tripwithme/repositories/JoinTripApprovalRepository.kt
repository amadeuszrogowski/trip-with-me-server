package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.JoinTripApproval
import org.springframework.data.mongodb.repository.MongoRepository

interface JoinTripApprovalRepository : MongoRepository<JoinTripApproval, String> {
    fun findAllByOrganizerId(participantId: String) : List<JoinTripApproval>
    fun findByParticipantIdAndTripId(participantId: String, tripId: String) : JoinTripApproval
}