package io.amicolon.tripwithme.util

import java.util.*

interface FindByName<T, ID> {
    fun findByName(name: ID) : Optional<T>
}