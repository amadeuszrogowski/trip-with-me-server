package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
data class Profile(
        @Id val id: String? = null,
        var name: String? = null,
        var surname: String? = null,
        var img: String? = null,
        var aboutMe: String? = null,
        var location: Place? = null,
        // TODO: flatten travel info
        var visitedCountries: Set<Country> = mutableSetOf(),
        var knownLanguages: Set<Language> = mutableSetOf(),
        var interests: Set<Interest> = mutableSetOf(),
        val memberSince: LocalDate = LocalDate.now(),
        var personalInfo: PersonalInfo = PersonalInfo(),
        var contactInfo: ContactInfo = ContactInfo()
)

data class PersonalInfo(
        var dateOfBirth: LocalDate? = null,
        var gender: Gender? = null,
        var university: University? = null,
        var course: Course? = null
)

data class ContactInfo(
        var phoneNumber: String? = null,
        var emailAddress: String? = null,
        var facebookAccountUrl: String? = null
)

@Document
data class Gender (
        @Id val id: String? = null,
        val name: String
)

@Document
data class Course (
        @Id val id: String? = null,
        val name: String
)

@Document
data class University (
        @Id val id: String? = null,
        val name: String
)