package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.Trip

interface SortService {
    fun sortBy(trips: List<Trip>, sortStrategy: String): List<Trip>
}