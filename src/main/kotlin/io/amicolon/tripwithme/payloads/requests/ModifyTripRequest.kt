package io.amicolon.tripwithme.payloads.requests

data class ModifyTripRequest(
        val description: String,
        val title: String,
        val budget: Int,
        val limit: Int,
        val img: String,
        val dateFrom: String,
        val dateTo: String,
        val organizerId: String,
        val destinationType: String,
        val countries: String?,
        val cities: String?,
        val activities: String?
)