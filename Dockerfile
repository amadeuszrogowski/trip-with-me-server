FROM openjdk:11-jre-slim

COPY build/libs/trip-with-me-1.0.0.jar .

EXPOSE 8080

CMD java -jar trip-with-me-1.0.0.jar