package io.amicolon.tripwithme.payloads.responses

import io.amicolon.tripwithme.domain.Trip

data class GetTripResponse(
        val trip: Trip,
        val joinState: String,
        val owner: Boolean
)