package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Country
import io.amicolon.tripwithme.domain.Place

interface DestinationsService {
    fun getCountriesFromStringList(countries: String): Set<Country>
    fun getPlacesFromStringList(places: String): Set<Place>
}