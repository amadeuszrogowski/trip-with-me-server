package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Destinations
import io.amicolon.tripwithme.domain.Interest
import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.payloads.requests.FetchTripsRequest
import io.amicolon.tripwithme.payloads.requests.ModifyTripRequest
import io.amicolon.tripwithme.payloads.requests.UpdateTripRequest
import io.amicolon.tripwithme.payloads.responses.GetTripResponse
import io.amicolon.tripwithme.repositories.InterestRepository
import io.amicolon.tripwithme.repositories.ProfileRepository
import io.amicolon.tripwithme.repositories.TripRepository
import io.amicolon.tripwithme.services.utils.FilterService
import io.amicolon.tripwithme.services.utils.JoinTripStateService
import io.amicolon.tripwithme.services.utils.SortService
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class TripServiceImpl(
        private val tripRepository: TripRepository,
        private val profileRepository: ProfileRepository,
        private val filterService: FilterService,
        private val sortService: SortService,
        private val joinTripStateService: JoinTripStateService,
        private val destinationsService: DestinationsService,
        private val interestRepository: InterestRepository
) : TripService {

    override fun getAll(): List<Trip> = tripRepository.findAll()

    override fun getFilteredAndSorted(fetchTripsRequest: FetchTripsRequest): List<Trip> {

        return if (fetchTripsRequest.startDate.isNullOrEmpty()) {
            sortService.sortBy(tripRepository.findAll(), fetchTripsRequest.sortStrategy)
        } else {
            val filteredTrips = filterService.filterBy(tripRepository.findAll(), fetchTripsRequest)
            sortService.sortBy(filteredTrips, fetchTripsRequest.sortStrategy)
        }
    }

    override fun getById(tripId: String, profileId: String): GetTripResponse {
        val trip = tripRepository.findById(tripId).get()
        trip.viewCounter++
        val savedTrip = tripRepository.save(trip)
        val joinTripState = joinTripStateService.getJoinTripState(savedTrip, profileId)
        val isOwner = savedTrip.organizer.id == profileId.replace("\"", "")
        return GetTripResponse(savedTrip, joinTripState, isOwner)
    }

    override fun create(modifyTripRequest: ModifyTripRequest): Trip {
        modifyTripRequest.let {
            return tripRepository.save(
                    Trip(
                            organizer = profileRepository.findById(it.organizerId).get(),
                            description = it.description,
                            title = it.title,
                            budget = it.budget,
                            img = it.img,
                            createdAt = LocalDate.now(),
                            startDate = LocalDate.parse(it.dateFrom),
                            finishDate = LocalDate.parse(it.dateTo),
                            viewCounter = 0,
                            requirements = getActivitiesFromStringList(it.activities),
                            participants = mutableSetOf(),
                            sizeLimit = it.limit,
                            destinations = createDestinations(it.destinationType, it.cities, it.countries)
                    )
            )
        }
    }

    fun getActivitiesFromStringList(places: String?): Set<Interest> {
        return places!!.split(",").asSequence()
                .map { interestRepository.findByName(it).get() }
                .toSet()
    }

    private fun createDestinations(destinationType: String, cities: String?, countries: String?): Destinations {
        return if (destinationType == "cities") {
            Destinations(
                    type = destinationType,
                    countries = emptySet(),
                    places = destinationsService.getPlacesFromStringList(cities!!)
            )
        } else {
            Destinations(
                    type = destinationType,
                    countries = destinationsService.getCountriesFromStringList(countries!!),
                    places = emptySet()
            )
        }
    }

    override fun getById(id: String): Trip {
        return tripRepository.findById(id).get()
    }

    override fun updateById(id: String, updateTripRequest: UpdateTripRequest): Trip {

        val trip = tripRepository.findById(id).get()

        trip.apply {
            title = updateTripRequest.title
            budget = Integer.valueOf(updateTripRequest.budget)
            img = updateTripRequest.img
            description = updateTripRequest.description
            requirements = updateTripRequest.activities!!.split(",").asSequence()
                    .map { interestRepository.findByName(it).get() }
                    .toSet()
        }

        return tripRepository.save(trip)
    }

    override fun insert(obj: Trip): Trip {
        return TODO("not implemented")
    }

    override fun update(obj: Trip): Trip {
        return TODO("not implemented")
    }

    override fun deleteById(id: String): Optional<Trip> {
        return TODO("not implemented")
    }
}