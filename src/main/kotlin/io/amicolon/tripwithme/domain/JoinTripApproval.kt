package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class JoinTripApproval(
        @Id val id: String? = null,
        val organizerId: String,
        val tripId: String,
        val participantId: String,
        var isPending: Boolean
)