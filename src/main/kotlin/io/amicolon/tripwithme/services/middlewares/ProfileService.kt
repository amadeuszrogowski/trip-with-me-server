package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Profile
import io.amicolon.tripwithme.payloads.requests.UpdateProfileRequest
import io.amicolon.tripwithme.payloads.responses.ProfileResponse
import io.amicolon.tripwithme.util.BasicCrud

interface ProfileService : BasicCrud<Profile, String> {
    fun retrieveById(profileId: String, currentProfileId: String): ProfileResponse
    fun updateById(id: String, updateProfileRequest: UpdateProfileRequest) : Profile
}