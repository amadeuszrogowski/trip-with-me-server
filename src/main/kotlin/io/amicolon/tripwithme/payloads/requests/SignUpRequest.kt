package io.amicolon.tripwithme.payloads.requests

data class SignUpRequest(
        val username: String,
        val email: String,
        val password: String
)