package io.amicolon.tripwithme.payloads.requests

data class UpdateTripRequest(
        val img: String,
        val title: String,
        val description: String,
        val budget: String,
        val activities: String?
)