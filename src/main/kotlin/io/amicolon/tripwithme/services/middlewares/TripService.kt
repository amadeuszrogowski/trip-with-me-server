package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.payloads.requests.FetchTripsRequest
import io.amicolon.tripwithme.payloads.requests.ModifyTripRequest
import io.amicolon.tripwithme.payloads.requests.UpdateTripRequest
import io.amicolon.tripwithme.payloads.responses.GetTripResponse
import io.amicolon.tripwithme.util.BasicCrud

interface TripService : BasicCrud<Trip, String> {
    fun create(modifyTripRequest: ModifyTripRequest): Trip
    fun getFilteredAndSorted(fetchTripsRequest: FetchTripsRequest): List<Trip>
    fun getById(tripId: String, profileId: String): GetTripResponse
    fun updateById(id: String, updateTripRequest: UpdateTripRequest) : Trip
}