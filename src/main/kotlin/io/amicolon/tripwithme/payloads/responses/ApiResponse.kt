package io.amicolon.tripwithme.payloads.responses

data class ApiResponse(
        val success: Boolean,
        val message: String
)