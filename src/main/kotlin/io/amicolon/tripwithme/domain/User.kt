package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class User(
        @Id val id: String? = null,
        var username: String,
        var email: String,
        var password: String,
        @DBRef(lazy = true) var roles: Set<Role> = emptySet(),
        // FIXME: change to val after removing bootstrap code
        @DBRef var profile: Profile = Profile()
)

@Document
data class Role (
        @Id val id: String? = null,
        val role: String
)