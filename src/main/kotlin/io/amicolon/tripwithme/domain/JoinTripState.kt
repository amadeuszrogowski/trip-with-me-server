package io.amicolon.tripwithme.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class JoinTripState(
        @Id val id: String? = null,
        val profileId: String,
        val tripId: String,
        val organizerId: String,
        var state: State
)

enum class State {
    PARTICIPATE_IN,
    PENDING,
    APPROVED,
    DECLINED;
}
