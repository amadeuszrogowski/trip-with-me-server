package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.Trip

interface JoinTripStateService {
    fun getJoinTripState(trip: Trip, profileId: String) : String
    fun isContactDataVisible(requestedProfileId: String, currentProfileId: String) : Boolean
    fun handleParticipateIn(tripId: String, profileId: String) : String
    fun handleApprove(tripId: String, profileId: String)
    fun handleDecline(tripId: String, profileId: String)
}