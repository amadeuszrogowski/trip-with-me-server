package io.amicolon.tripwithme.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class AppException : Exception {

    constructor(message: String, e: Exception?): super(message, e)

    constructor(message: String): super(message)

    constructor(e: Exception): super(e)
}