package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.payloads.requests.ChangeApprovalStateRequest
import io.amicolon.tripwithme.services.utils.JoinTripStateService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/joinstate")
class JoinTripStateController(
        private val joinTripStateService: JoinTripStateService
) {

    @PostMapping("/participate")
    fun participateIn(@Valid @ModelAttribute changeApprovalStateRequest: ChangeApprovalStateRequest): ResponseEntity<Any> {
        return ResponseEntity.ok(joinTripStateService.handleParticipateIn(changeApprovalStateRequest.tripId, changeApprovalStateRequest.profileId))
    }

    @PostMapping("/approve")
    fun approve(@Valid @ModelAttribute changeApprovalStateRequest: ChangeApprovalStateRequest): ResponseEntity<Any> {
        return ResponseEntity.ok(joinTripStateService.handleApprove(changeApprovalStateRequest.tripId, changeApprovalStateRequest.profileId))
    }

    @PostMapping("/decline")
    fun decline(@Valid @ModelAttribute changeApprovalStateRequest: ChangeApprovalStateRequest): ResponseEntity<Any> {
        return ResponseEntity.ok(joinTripStateService.handleDecline(changeApprovalStateRequest.tripId, changeApprovalStateRequest.profileId))
    }
}
