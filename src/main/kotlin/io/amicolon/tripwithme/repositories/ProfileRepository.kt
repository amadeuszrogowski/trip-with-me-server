package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Profile
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface ProfileRepository : MongoRepository<Profile, String> {

    override fun findById(id: String) : Optional<Profile>
    fun findBySurname(surname: String) : Optional<Profile>
}