package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.config.security.jwt.JwtTokenProvider
import io.amicolon.tripwithme.domain.User
import io.amicolon.tripwithme.exceptions.AppException
import io.amicolon.tripwithme.payloads.requests.LoginRequest
import io.amicolon.tripwithme.payloads.requests.SignUpRequest
import io.amicolon.tripwithme.payloads.responses.ApiResponse
import io.amicolon.tripwithme.payloads.responses.JwtAuthenticationResponse
import io.amicolon.tripwithme.repositories.ProfileRepository
import io.amicolon.tripwithme.repositories.RoleRepository
import io.amicolon.tripwithme.repositories.UserRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/auth")
class AuthController(
        private val authenticationManager: AuthenticationManager,
        private val userRepository: UserRepository,
        private val roleRepository: RoleRepository,
        private val profileRepository: ProfileRepository,
        private val passwordEncoder: PasswordEncoder,
        private val jwtTokenProvider: JwtTokenProvider
) {

    @PostMapping("/signin")
    fun authenticateUser(@Valid @ModelAttribute loginRequest: LoginRequest): ResponseEntity<Any> {

        val auth = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginRequest.usernameOrEmail,
                        loginRequest.password
                )
        )

        SecurityContextHolder.getContext().authentication = auth

        val jwt = jwtTokenProvider.generateToken(auth)
        val user = userRepository.findByUsername(loginRequest.usernameOrEmail).get()
        return ResponseEntity.ok()
                .body(JwtAuthenticationResponse(
                        accessToken = jwt,
                        userId = user.id!!,
                        profileId = user.profile.id!!
                ))
    }

    @PostMapping("/signup")
    fun registerUser(@Valid @RequestBody signUpRequest: SignUpRequest): ResponseEntity<Any> {

        if (userRepository.existsByUsername(signUpRequest.username)) {
            return ResponseEntity(ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST)
        }

        if (userRepository.existsByEmail(signUpRequest.email)) {
            return ResponseEntity(ApiResponse(false, "Email address already taken!"), HttpStatus.BAD_REQUEST)
        }

        val user = User(username = signUpRequest.username, email = signUpRequest.email, password = signUpRequest.password)
        user.password = passwordEncoder.encode(user.password)

        val role = roleRepository.findByRole("ROLE_USER")
                .orElseThrow { AppException("User role not set") }

        user.roles = Collections.singleton(role)

        val persistedProfile = profileRepository.save(user.profile)
        user.profile = persistedProfile
        val result = userRepository.save(user)

        val uri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/api/users/{username}")
                .buildAndExpand(result.username)
                .toUri()

        return ResponseEntity.created(uri).body(ApiResponse(true, "User registered successfully"))
    }
}

