package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.payloads.requests.FetchTripsRequest
import io.amicolon.tripwithme.payloads.requests.ModifyTripRequest
import io.amicolon.tripwithme.payloads.requests.UpdateTripRequest
import io.amicolon.tripwithme.services.middlewares.TripService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class TripController(private val tripService: TripService) {

    @GetMapping("/trips")
    fun getFilteredAndSorted(@Valid @ModelAttribute fetchTripsRequest: FetchTripsRequest): ResponseEntity<Any> {
        return ResponseEntity.ok().body(tripService.getFilteredAndSorted(fetchTripsRequest))
    }

    @GetMapping("/trip/{tripId}")
    fun getById(@PathVariable tripId: String, @RequestParam profileId: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(tripService.getById(tripId, profileId))
    }

    @PostMapping("/trip/{id}")
    fun updateById(@PathVariable id: String, @Valid @ModelAttribute updateTripRequest: UpdateTripRequest) {
        ResponseEntity.ok().body(tripService.updateById(id, updateTripRequest))
    }

    @PostMapping("/trip/new")
    fun create(@Valid @ModelAttribute modifyTripRequest: ModifyTripRequest): ResponseEntity<Any> {
        return ResponseEntity.ok().body(tripService.create(modifyTripRequest))
    }
}