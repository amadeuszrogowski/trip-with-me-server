package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.services.utils.JoinTripApprovalService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/box")
class JoinTripApprovalController(
        private val joinTripApprovalService: JoinTripApprovalService
) {

    @GetMapping("/all")
    fun getAll(@RequestParam profileId: String): ResponseEntity<Any> {
        return ResponseEntity.ok(joinTripApprovalService.getAll(profileId))
    }

}