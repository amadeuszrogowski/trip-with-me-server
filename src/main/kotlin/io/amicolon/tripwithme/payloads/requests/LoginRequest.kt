package io.amicolon.tripwithme.payloads.requests

data class LoginRequest(
        val usernameOrEmail: String,
        val password: String
)