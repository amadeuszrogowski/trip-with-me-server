package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.payloads.requests.FetchTripsRequest
import io.amicolon.tripwithme.repositories.CountryRepository
import io.amicolon.tripwithme.repositories.InterestRepository
import io.amicolon.tripwithme.repositories.PlaceRepository
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class FilterServiceImpl(
        private val placeRepository: PlaceRepository,
        private val countryRepository: CountryRepository,
        private val interestRepository: InterestRepository
) : FilterService {

    override fun filterBy(trips: List<Trip>, request: FetchTripsRequest): List<Trip> {
        val startDate = LocalDate.parse(request.startDate)
        val finishDate = LocalDate.parse(request.finishDate)

        return handleLocation(trips, request)
                .filter { isInDateRange(it, startDate, finishDate) }
                .filter { isInBudgetRange(it, request.minBudget!!, request.maxBudget!!) }
                .filter { isInPeopleLimitRange(it, request.minPeople!!, request.maxBudget!!) }
                .filter { hasAnyActivity(it, request.activities) }
                .toMutableList()
    }

    fun handleLocation(trips: List<Trip>, request: FetchTripsRequest): Sequence<Trip> {

        return when (request.searchType) {
            "text" -> trips.asSequence().filter { containsSpecialText(it, request.text!!) }
            "places" -> trips.asSequence().filter { placesExist(it, request.places!!) }
            "countries" -> trips.asSequence().filter { countriesExist(it, request.countries!!) }
            else -> trips.asSequence()
        }
    }

    fun placesExist(trip: Trip, places: String): Boolean {
        val placesValues = places.split(",")
        return placesValues.asSequence()
                .all { (trip.destinations.places.contains(placeRepository.findByName(it).get())) }
    }

    fun countriesExist(trip: Trip, countries: String): Boolean {
        val countriesValues = countries.split(",")
        return countriesValues.asSequence()
                .all { country ->
                    return if (trip.destinations.type == "countries") {
                        trip.destinations.countries.contains(countryRepository.findByName(country).get())
                    } else {
                        trip.destinations.places.asSequence()
                                .map { it.country }
                                .contains(countryRepository.findByName(country).get())
                    }
                }
    }

    fun containsSpecialText(trip: Trip, text: String): Boolean {
        val searchValues = text.replace(" ", "").split(",")
        // TODO: check countries names and places names
        return searchValues.asSequence()
                .all { trip.description.contains(it) || trip.title.contains(it) }
    }

    fun isInDateRange(trip: Trip, requestStartDate: LocalDate, requestFinishDate: LocalDate): Boolean {
        val tripStartDate = trip.startDate
        val finishStartDate = trip.finishDate
        return (tripStartDate.isEqual(requestStartDate) || tripStartDate.isAfter(requestStartDate)) &&
                (finishStartDate.isEqual(requestFinishDate) || finishStartDate.isBefore(requestFinishDate))
    }

    fun isInBudgetRange(trip: Trip, MIN_BUDGET: Int, MAX_BUDGET: Int): Boolean {
        return trip.budget in MIN_BUDGET..MAX_BUDGET
    }

    fun isInPeopleLimitRange(trip: Trip, MIN_LIMIT: Int, MAX_LIMIT: Int): Boolean {
        return trip.sizeLimit in MIN_LIMIT..MAX_LIMIT
    }

    private fun hasAnyActivity(trip: Trip, activities: String?) : Boolean{
        return if (activities.isNullOrBlank()) {
            true
        } else {
            activities!!.split(",").asSequence()
                    .any { (trip.requirements.contains(interestRepository.findByName(it).get())) }
        }
    }

}