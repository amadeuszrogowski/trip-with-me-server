package io.amicolon.tripwithme.payloads.requests

data class UpdateProfileRequest(
        val name: String,
        val surname: String,
        val aboutMe: String,
        val img: String,
        val phoneNumber: String,
        val email: String,
        val facebook: String,
        val dateOfBirth: String,
        val gender: String,
        val course: String,
        val university: String,
        val countries: String,
        val languages: String,
        val location: String,
        val activities: String?
)