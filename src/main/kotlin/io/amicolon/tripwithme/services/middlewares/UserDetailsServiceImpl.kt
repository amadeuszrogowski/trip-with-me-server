package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.config.security.UserPrincipal
import io.amicolon.tripwithme.repositories.UserRepository
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl(private val userRepository: UserRepository) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(usernameOrEmail: String): UserDetails {

        val user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow { UsernameNotFoundException("User not found with username or email : $usernameOrEmail") }

        return UserPrincipal.create(user)
    }

    fun loadUserById(id: String?): UserDetails {

        val user = userRepository.findById(id!!).orElseThrow { UsernameNotFoundException("User not found with id : $id") }

        return UserPrincipal.create(user)
    }
}