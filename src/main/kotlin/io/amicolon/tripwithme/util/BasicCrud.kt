package io.amicolon.tripwithme.util

import java.util.*

interface BasicCrud<T, ID> {
    fun getAll(): List<T>
    fun getById(id: ID): T
    fun insert(obj: T): T
    fun update(obj: T): T
    fun deleteById(id: ID): Optional<T>
}