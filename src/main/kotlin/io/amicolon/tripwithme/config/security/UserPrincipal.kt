package io.amicolon.tripwithme.config.security

import com.fasterxml.jackson.annotation.JsonIgnore
import io.amicolon.tripwithme.domain.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class UserPrincipal(
        private val id: String? = null,
        private val username: String,
        @JsonIgnore private val password: String,
        @JsonIgnore private val email: String,
        private val authorities: Collection<out GrantedAuthority>
) : UserDetails {

    companion object {
        fun create(user: User): UserPrincipal {

            val authorities: List<GrantedAuthority> = user.roles!!.asSequence()
                    .map { SimpleGrantedAuthority(it.role) }
                    .toList()

            return UserPrincipal(
                    id = user.id,
                    username = user.username,
                    email = user.email,
                    password = user.password,
                    authorities = authorities
            )
        }
    }

    fun getId() = id

    override fun getUsername() = username

    override fun getPassword() = password

    override fun getAuthorities() = authorities

    override fun isEnabled() = true

    override fun isCredentialsNonExpired() = true

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true
}