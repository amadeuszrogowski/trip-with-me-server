package io.amicolon.tripwithme.services.utils

import io.amicolon.tripwithme.domain.JoinTripState
import io.amicolon.tripwithme.domain.State
import io.amicolon.tripwithme.domain.Trip
import io.amicolon.tripwithme.repositories.JoinTripStateRepository
import io.amicolon.tripwithme.repositories.ProfileRepository
import io.amicolon.tripwithme.repositories.TripRepository
import org.springframework.stereotype.Service

@Service
class JoinTripStateServiceImpl(
        private val joinTripStateRepository: JoinTripStateRepository,
        private val joinTripApprovalService: JoinTripApprovalService,
        private val tripRepository: TripRepository,
        private val profileRepository: ProfileRepository
) : JoinTripStateService {

    override fun getJoinTripState(trip: Trip, profileId: String): String {
        val joinTripState = joinTripStateRepository.findByTripIdAndProfileId(trip.id!!, profileId)
        return joinTripState?.state?.name ?: State.PARTICIPATE_IN.name
    }

    override fun isContactDataVisible(requestedProfileId: String, currentProfileId: String): Boolean {
        return if (requestedProfileId == currentProfileId) {
            true
        } else (joinTripStateRepository.findByOrganizerIdAndProfileId(requestedProfileId, currentProfileId)?.state == State.APPROVED
                || joinTripStateRepository.findByOrganizerIdAndProfileId(currentProfileId, requestedProfileId)?.state == State.APPROVED)
    }

    override fun handleParticipateIn(tripId: String, profileId: String): String {
        val savedState = joinTripStateRepository.save(
                JoinTripState(
                        profileId = profileId,
                        organizerId = tripRepository.findById(tripId).get().organizer.id!!,
                        tripId = tripId,
                        state = State.PENDING
                )
        )

        joinTripApprovalService.sentRequestToBox(
                tripId = tripId,
                participantId = profileId,
                organizerId = tripRepository.findById(tripId).get().organizer.id!!
        )

        return savedState.state.name
    }

    override fun handleApprove(tripId: String, profileId: String) {
        handleDecision(tripId, profileId, State.APPROVED)

        joinTripApprovalService.deleteMessage(tripId, profileId)

        val profile = profileRepository.findById(profileId).get()
        var trip = tripRepository.findById(tripId).get()
        trip.participants.add(profile)
        tripRepository.save(trip)
    }

    override fun handleDecline(tripId: String, profileId: String) {
        handleDecision(tripId, profileId, State.DECLINED)

        joinTripApprovalService.deleteMessage(tripId, profileId)
    }

    private fun handleDecision(tripId: String, profileId: String, state: State) {
        val joinTripState = joinTripStateRepository.findByTripIdAndProfileId(tripId, profileId)
        joinTripState!!.state = state
        joinTripStateRepository.save(joinTripState)
    }
}