package io.amicolon.tripwithme.controllers

import io.amicolon.tripwithme.payloads.requests.UpdateProfileRequest
import io.amicolon.tripwithme.services.middlewares.ProfileService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/profile")
class ProfileController(private val profileService: ProfileService) {

    @GetMapping("/all")
    fun getAll(): ResponseEntity<Any> {
        return ResponseEntity.ok(profileService.getAll())
    }

    @GetMapping("/{profileId}")
    fun getById(@PathVariable profileId: String, @RequestParam currentProfileId: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(profileService.retrieveById(profileId, currentProfileId))
    }

    @PostMapping("/{id}")
    fun updateById(@PathVariable id: String, @Valid @ModelAttribute updateProfileRequest: UpdateProfileRequest) {
        ResponseEntity.ok().body(profileService.updateById(id, updateProfileRequest))
    }
}