package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Country
import io.amicolon.tripwithme.domain.Place
import io.amicolon.tripwithme.util.FindByName
import org.springframework.data.mongodb.repository.MongoRepository

interface PlaceRepository : MongoRepository<Place, String>, FindByName<Place, String>

interface CountryRepository : MongoRepository<Country, String>, FindByName<Country, String>