package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.JoinTripState
import org.springframework.data.mongodb.repository.MongoRepository

interface JoinTripStateRepository : MongoRepository<JoinTripState, String> {
    fun findByTripIdAndProfileId(tripId: String, profileId: String) : JoinTripState?
    fun findByOrganizerIdAndProfileId(organizerId: String, participantId: String) : JoinTripState?
}