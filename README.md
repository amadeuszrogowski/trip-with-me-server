### About Trip With Me
`Author: Amadeusz Rogowski`    
`Date of creation: August - November 2019`  

Client codebase: https://bitbucket.org/amadeuszrogowski/trip-with-me-client/src/master/  
Server codebase: https://bitbucket.org/amadeuszrogowski/trip-with-me-server/src/master/ 
      
Trip With Me is an application created for my bachelor thesis.
This application allows you to add trip ads and offers the opportunity to participate in these tours for others users.
The system allows you to organize trips for two people, as well as a larger group.
The system allows the organizer to enter a set of information about the trip, such as date range, destination, or the required budget.
Trip With Me allows you to search efficiently ads, using a set of filters, and then sorting the results.
Additionally, it enables contact by phone or e-mail between the participant of the trip and the organizer.
    
### Run application
To start up application just run in commandline __`docker-compose up`__ in your root directory.  
Go to your browser to http://localhost:3000/login and login to application, you can use two accounts which are predefined: 
- username: joesmith, password: joesmith 
- username: danielwilliams, password: danielwilliams  

Also you can create a user, by going to http://localhost:3000/singup, after that you can log in with new credentials and fill your profile data.

### Main view
![main view](/image/views/main.png)  
This view is main view in application. It displays trips previews (title, destinations, budget, dates, squad and description).
You can search trips you desire by choosing countries or places you want to travel to. You can also choose dates range, budget range, squad limit, and some activities you are interested in.
Results can by sorted by newest, by popularity (with most ad's views), by price, and you can check last minute trips.   


### Profile view
![profile view](/image/views/profile.png)   
This view represents user's profile. User can define their personal details such as age or university.
Also, can specify their contact details, travel details i.a. countries they traveled to, description and interests.
Profile is editable.


### Trip view
![trip view](/image/views/trip.png)  
This view represents trip's ad. There is some photo, organizer and other trip details.
You can also see list of participants. Any user can make a participation request, then organizer of trip can approve or decline request.
Requests are stored in an organizer's box. When request is approved user joins trip, otherwise they don't and cannot make a request again for this specific trip. 


### Additional comments
There are two accounts predefined with credentials: joesmith:joesmith and danielwilliams:daniellwilliams.  
Use this format date working with forms: yyyy-mm-dd e.g 1994-10-12.  
`Disclaimer:` I'm not front-end developer, CSS is not my strong forte, so styling leaves a lot to be desired.    
`Disclaimer:` Application uses dummy data, just to show you idea of this platform, dummy data are loaded and the startup of application. 

### Technology stack
![kotlin logo](/image/tech/kotlin.png)
![spring logo](/image/tech/spring.png)
![java logo](/image/tech/java.jpg)
![docker logo](/image/tech/docker.jpg)
![mongodb logo](image/tech/mongo.png)
![gradle logo](image/tech/gradle.png)
![react logo](image/tech/react.png)
![js logo](image/tech/js.png)