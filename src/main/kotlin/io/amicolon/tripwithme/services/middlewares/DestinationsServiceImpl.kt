package io.amicolon.tripwithme.services.middlewares

import io.amicolon.tripwithme.domain.Country
import io.amicolon.tripwithme.domain.Place
import io.amicolon.tripwithme.repositories.CountryRepository
import io.amicolon.tripwithme.repositories.PlaceRepository
import org.springframework.stereotype.Service

@Service
class DestinationsServiceImpl(
        private val countryRepository: CountryRepository,
        private val placeRepository: PlaceRepository
) : DestinationsService {

    override fun getCountriesFromStringList(countries: String): Set<Country> {
        return countries.split(",").asSequence()
                .map { countryRepository.findByName(it).get() }
                .toSet()
    }

    override fun getPlacesFromStringList(places: String): Set<Place> {
        return places.split(",").asSequence()
                .map { placeRepository.findByName(it).get() }
                .toSet()
    }
}