package io.amicolon.tripwithme.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException(resource: String, id: String)
    : RuntimeException(String.format("Resource %s with %s not found", resource, id))