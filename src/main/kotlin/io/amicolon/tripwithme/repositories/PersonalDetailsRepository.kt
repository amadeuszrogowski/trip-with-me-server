package io.amicolon.tripwithme.repositories

import io.amicolon.tripwithme.domain.Course
import io.amicolon.tripwithme.domain.Gender
import io.amicolon.tripwithme.domain.University
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface GenderRepository : MongoRepository<Gender, String> {

    fun findByName(name: String): Optional<Gender>
}

interface UniversityRepository : MongoRepository<University, String> {

    fun findByName(name: String): Optional<University>
}

interface CourseRepository : MongoRepository<Course, String> {

    fun findByName(name: String): Optional<Course>
}